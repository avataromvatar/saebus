/*
 * saebus_common.h
 *
 *  Created on: 17 сент. 2019 г.
 *      Author: avatar
 *
 *      Структура sSAEvent хранит все информацию для работы с событиями
 *      пользователь иницилизирует структуру sSAEvent и хранит у себя после SAE_registerPublisher в
 *      sSAEvent заполняется дискриптор по которому будет обеспечен быстрый доступ к оповещению
 *
 *	Внимание!
 *	 Если событи срабатывает до того как было прочитано(если оно не моментально),
 *	 то данные события просто перезапишутся
 *
 *
 *      Использование:
 *      1 Издатель иницилизирует событие и получает дискриптор события
 *      2 оповещает подписчиков через дискриптор
 *
 */

#ifndef SAEBUS_COMMON_H_
#define SAEBUS_COMMON_H_

#include "stdint.h"

#ifndef SAEB_MAX_EVENT
#define SAEB_MAX_EVENT 32
#endif

#ifndef SAEB_MAX_FIFO_EVENT
#define SAEB_MAX_FIFO_EVENT 32
#endif

//#define UID_EVENT(module,type,id,flag_moment) ((flag_moment&0x01)<<31 | (module&0xFF)<<17 | (type&0x3F)<<25 | (id&0xFF)<<9)
//#define ENAME_EVENT(module,type,id,flag_moment) module##_##type##_##id##_##flag_moment = ((flag_moment&0x01)<<31 | (module&0xFF)<<17 | (type&0x3F)<<25 | (id&0xFF)<<9)
#define UID_EVENT(module,type,id,flag_moment) ((flag_moment&0x01) | (module&0xFF)<<7 | (type&0x3F)<<1 | (id&0xFF)<<15)
#define ENAME_EVENT(module,type,id,flag_moment) module##_##type##_##id##_##flag_moment = ((flag_moment&0x01) | ((module&0xFF)<<7) | ((type&0x3F)<<1) | ((id&0xFF)<<15))

#define NAME_EVENT(module,type,id,flag_moment) module##_##type##_##id##_##flag_moment
#define GET_UID_EVENT(uEvent_val) (uEvent_val>>15)
#define UID_EVENT_MASK 0xFFFFFE00
//#define GET_MODULE_FROM_UID_EVENT(uid_event) ((uid_event>>24)&0xFF)
//#define GET_TYPE_FROM_UID_EVENT(uid_event) ((uid_event>>16)&0xFF)
//#define GET_NUM_FROM_UID_EVENT(uid_event) (uid_event&0xFFFF)


typedef enum
{
	NO_ERROR = 0,
	EVENT_NOT_REGISTERED,
	EVENT_ALREADY_REGISTERED,
	PUBLISHER_NOT_REGISTERED,
	CORRUPTED_ID_EVENT,
	NOT_ENOUGH_MEM,
	CORRUPTED_DESCRIPTOR_SAE,
	NO_SUBSCRIBER,
	NULL_POINTER_TO_SUBSCRIBER,
	NULL_POINTER_TO_EVENT,
	SUBSCRIBER_ALREADY_REGISTERED,
	NO_EVENT_IN_FIFO,
	FIFO_FULL

}eSAESysErrorFlags;


typedef enum
{
	NO_PUBLISHER = 0,
	PUBLISHER_REGISTERED = 0x01, //издатель зарегестрирован
	INSTANT_CALL = 0x02, //мгновенный вызов
	RELEASED = 0x04, //издано
	DELIVERED = 0x08 //доставленно

}eSAEFlags;


typedef union
{
	uint8_t ud8[8];
	int8_t id8[8];
	uint16_t ud16[4];
	int16_t id16[4];
	uint32_t ud32[2];
	int32_t id32[2];
	float fd[2];
	void *vd[2];
}uSAEData;



typedef union
{
	struct {
		uint32_t momenatry:1;
		uint32_t type:6;
		uint32_t module:8;
		uint32_t id:8;
		uint32_t user_data:6; //поле может быть использовано как угодно оно не используется для uid event(UID_EVENT)
		uint32_t NOT_USE_PLZ:3;
	}event;
	uint32_t val;
}uEvent;

typedef void (*sae_handling)(uEvent event,uSAEData data);
typedef struct
{
	uEvent event;
	uint32_t eve_descriptor;
	uint32_t num_handling;
	uint32_t max_num_handling;
	sae_handling *handling;
//	const char *eve_info;
//	const char *ret_info;
}sSAEvent;



/**
 * Регестрирует событие Издателя, устанавливает в нем eve_descriptor и возращает его же в случае успешной регистрации иначе 0
 * @param sae структура sSAEvent
 * @param error при ret=0 описывает ошибку
 * @return eve_descriptor если все хорошо, иначе 0
 */
uint32_t SAE_registerPublisher(uint32_t uevent_val, uint32_t max_num_listeners,eSAESysErrorFlags *error);

/**
 * Оповестить согласно очереди событий
 * @param event_desc
 * @param data
 * @return eSAESysErrorFlags
 */
eSAESysErrorFlags SAE_notifySubscribers(uint32_t event_desc,uSAEData data);

/**
 * Оповестить моментально
 * @param event
 * @param data
 * @return
 */
eSAESysErrorFlags SAE_notifySubscribersMomentary(uint32_t event_desc,uSAEData data);

/**
 * добавить подписчика на событие
 * @param event
 * @param cb
 * @return eSAESysErrorFlags
 */
eSAESysErrorFlags SAE_addSubscribe(uint32_t event_val,sae_handling cb);

/**
 * Удаляет подписчика
 * @param event
 * @param cb
 * @return
 */
eSAESysErrorFlags SAE_removeSubscribe(uint32_t event_val,sae_handling cb);

/**
 * Отрабатывает очередь событий по 1 событию за вызов
 * @return если NO_ERROR то события в шине еще есть
 */
eSAESysErrorFlags SAE_UpdateByStep();

/**
 * Отрабатывает очередь событий сразу все
 * @return если NO_ERROR то события в шине еще есть
 */
eSAESysErrorFlags SAE_UpdateAll();

//
//uint32_t SAE_registerPublisher(sSAEContainer *saec,eSAESysErrorFlags *error); //return event_desc
//eSAESysErrorFlags SAE_notifySubscribers(uint32_t event_desc);
//
//eSAESysErrorFlags SAE_addSubscribe(uint32_t event,sae_subscriber cb);
//eSAESysErrorFlags SAE_removeSubscribe(uint32_t event,sae_subscriber cb);




#endif /* SAEBUS_COMMON_H_ */
