/*
 * saebus_common.c
 *
 *  Created on: 17 сент. 2019 г.
 *      Author: avatar
 */

#include "saebus_common.h"
#include "../fifo/fifo_c.h"
#include "stdlib.h"
#define SAE_DISCRIPTOR_BEGIN 0x0F

sSAEvent *h_sae_arr[SAEB_MAX_EVENT] =
{ 0 };
uint32_t h_num_sae = 0;
//sFIFO h_eve_fifo;
uint32_t h_sae_status = 0;

typedef struct
{
	//uint32_t eve_id;
	uint32_t eve_status;
	uSAEData data;
} _sSAEPack;

_sSAEPack h_res;
_sSAEPack h_res_update;
_sSAEPack h_eve_pack_arr[SAEB_MAX_EVENT] =
{ 0 };

uint32_t SAE_registerPublisher(uint32_t uevent_val, uint32_t max_num_listeners,
		eSAESysErrorFlags *error)
{
	if (!h_sae_status)
	{
//		h_eve_fifo.max_size = sizeof(_sSAEPack) * SAEB_MAX_FIFO_EVENT;
//		h_eve_fifo.fifo = (char*) eve_pack_arr;
//		h_eve_fifo.isEmpty = 1;
//		h_eve_fifo.pos_in = 0;
//		h_eve_fifo.pos_out = 0;
		h_sae_status = 1;
	}
	//if (sae)
	{
		if (uevent_val != 0)
		{
			for (int i = 0; i < h_num_sae; i++)
			{
				if (h_sae_arr[i]->event.val == uevent_val)
					return EVENT_ALREADY_REGISTERED;
			}
			if (h_num_sae < SAEB_MAX_EVENT)
			{
				h_sae_arr[h_num_sae] = malloc(sizeof(sSAEvent));
				memset(h_sae_arr[h_num_sae], 0, sizeof(sSAEvent));
				h_sae_arr[h_num_sae]->event.val = uevent_val;
				h_sae_arr[h_num_sae]->max_num_handling = max_num_listeners;
				h_sae_arr[h_num_sae]->num_handling = 0;
				h_sae_arr[h_num_sae]->handling = malloc(
						sizeof(sae_handling) * max_num_listeners);
				h_sae_arr[h_num_sae]->eve_descriptor = h_num_sae
						+ SAE_DISCRIPTOR_BEGIN;

				h_num_sae++;
				if (error)
					*error = NO_ERROR;
				return h_sae_arr[h_num_sae - 1]->eve_descriptor;
			}
			else
			{
				if (error)
					*error = NOT_ENOUGH_MEM;
				return 0;
			}
		}
	}
	*error = NULL_POINTER_TO_EVENT;
	return 0;
}
eSAESysErrorFlags SAE_notifySubscribers(uint32_t event_desc, uSAEData data)
{
	if (event_desc < SAE_DISCRIPTOR_BEGIN
			|| event_desc >= (SAE_DISCRIPTOR_BEGIN + h_num_sae))
		return CORRUPTED_DESCRIPTOR_SAE;

	uint32_t id = event_desc - SAE_DISCRIPTOR_BEGIN;
	if (!h_sae_arr[id]->event.event.momenatry)
	{
		h_eve_pack_arr[id].eve_status = 1;
		h_eve_pack_arr[id].data = data;

//	if (_fifo_push(&h_eve_fifo, (char*) &h_res, sizeof(h_res)) != ERROR_NO)
//		return FIFO_FULL;
	}
	else
	{
		for (uint32_t i = 0; i < h_sae_arr[id]->num_handling; i++)
			h_sae_arr[id]->handling[i](h_sae_arr[id]->event, data);
	}
	return NO_ERROR;

}
eSAESysErrorFlags SAE_notifySubscribersMomentary(uint32_t event_desc,
		uSAEData data)
{

	if (event_desc < SAE_DISCRIPTOR_BEGIN
			|| event_desc >= (SAE_DISCRIPTOR_BEGIN + h_num_sae))
		return CORRUPTED_DESCRIPTOR_SAE;
	uint32_t id = event_desc - SAE_DISCRIPTOR_BEGIN;

	for (uint32_t i = 0; i < h_sae_arr[id]->num_handling; i++)
		h_sae_arr[id]->handling[i](h_sae_arr[id]->event, data);

	return NO_ERROR;
}
eSAESysErrorFlags SAE_addSubscribe(uint32_t event_val, sae_handling cb)
{
	for (uint32_t i = 0; i < h_num_sae; i++)
	{
		if ((h_sae_arr[i]->event.val&UID_EVENT_MASK) == (event_val&UID_EVENT_MASK))
		{
			if (h_sae_arr[i]->num_handling < h_sae_arr[i]->max_num_handling)
			{
				h_sae_arr[i]->handling[h_sae_arr[i]->num_handling] = cb;
				h_sae_arr[i]->num_handling++;
				return NO_ERROR;
			}

		}
	}
	return EVENT_NOT_REGISTERED;
}
eSAESysErrorFlags SAE_removeSubscribe(uint32_t event_val, sae_handling cb)
{
	for (uint32_t i = 0; i < h_num_sae; i++)
	{
		if ((h_sae_arr[i]->event.val&UID_EVENT_MASK) == (event_val&UID_EVENT_MASK))
		{
			for (uint32_t i1 = 0; i1 < h_sae_arr[i]->num_handling; i1++)
			{
				if (h_sae_arr[i]->handling[i1] == cb)
				{
					h_sae_arr[i]->handling[i1] =
							h_sae_arr[i]->handling[h_sae_arr[i]->num_handling];
					h_sae_arr[i]->num_handling--;
					return NO_ERROR;
				}
			}

		}
	}
	return NO_SUBSCRIBER;
}

eSAESysErrorFlags SAE_UpdateByStep()
{
	static uint32_t last_eve_id = 0;
	if (last_eve_id >= SAEB_MAX_EVENT)
		last_eve_id = 0;
	for (int i = last_eve_id; i < SAEB_MAX_EVENT; i++)
	{
		if (h_eve_pack_arr[i].eve_status)
		{
			last_eve_id=i+1;
			for (uint32_t i1 = 0; i1 < h_sae_arr[i]->num_handling; i1++)
			{
				h_sae_arr[i]->handling[i1](h_sae_arr[i]->event,
						h_eve_pack_arr[i].data);
			}
			h_eve_pack_arr[i].eve_status = 0;
			return NO_ERROR;
		}
	}
	return NO_ERROR;
}

eSAESysErrorFlags SAE_UpdateAll()
{

		for (int i = 0; i < SAEB_MAX_EVENT; i++)
		{
			if (h_eve_pack_arr[i].eve_status)
			{

				for (uint32_t i1 = 0; i1 < h_sae_arr[i]->num_handling; i1++)
				{
					h_sae_arr[i]->handling[i1](h_sae_arr[i]->event,
							h_eve_pack_arr[i].data);
				}
				h_eve_pack_arr[i].eve_status = 0;

			}
		}
		return NO_ERROR;
}

//
//static sSAEContainer *_h_saebus[SAEB_MAX_EVENT] = { 0 };
//
////return event_desc
//uint32_t SAE_registerPublisher(sSAEContainer *saec, eSAESysErrorFlags *error) {
//
//	if (saec) {
//		if (saec->flags == NO_PUBLISHER) {
//			if (error)
//				*error = PUBLISHER_NOT_REGISTERED;
//			return 0;
//		}
//		if (saec->event == 0) {
//			if (error)
//				*error = EVENT_NOT_REGISTERED;
//			return 0;
//		}
//		int pos = -1;
//		for (int i1 = 0; i1 < SAEB_MAX_EVENT; i1++) {
//
//			if (_h_saebus[i1]) {
//				if (_h_saebus[i1]->event == saec->event) {
//
//					if (error)
//						*error = EVENT_ALREADY_REGISTERED;
//					return 0;
//				}
//				if (pos == -1) {
//					if (_h_saebus[i1]->event == 0
//							&& _h_saebus[i1]->flags == NO_PUBLISHER)
//						pos = i1;
//				}
//			}
//		}
//
//		if (pos != -1) {
//			_h_saebus[pos] = saec;
//			return pos + SAE_DISCRIPTOR_BEGIN;
//		} else {
//			if (error)
//				*error = NOT_ENOUGH_MEM;
//			return 0;
//		}
//
//	}
//
//}
//eSAESysErrorFlags SAE_notifySubscribers(uint32_t event_desc) {
//	uint32_t id = event_desc - SAE_DISCRIPTOR_BEGIN;
//	if (id < SAEB_MAX_EVENT) {
//		if(_h_saebus[id])
//		if (_h_saebus[id]->flags != NO_PUBLISHER)
//			for (int i = _h_saebus[id]->subsc_cont.len; i > 0; i--) {
//				if (_h_saebus[id]->subsc_cont.subscribers[i - 1])
//					_h_saebus[id]->subsc_cont.subscribers[i - 1](
//							_h_saebus[id]->event, _h_saebus[id]->data.type,
//							_h_saebus[id]->data.raw, _h_saebus[id]->data.len);
//				else
//					return NULL_POINTER_TO_SUBSCRIBER;
//			}
//	} else
//		return CORRUPTED_DESCRIPTOR_SAE;
//}
//
//eSAESysErrorFlags SAE_addSubscribe(uint32_t event, sae_subscriber cb) {
//	for (int i1 = 0; i1 < SAEB_MAX_EVENT; i1++) {
//		if(_h_saebus[i1])
//		if (_h_saebus[i1]->event == event) {
//			if (_h_saebus[i1]->flags != NO_PUBLISHER) {
//				if (_h_saebus[i1]->subsc_cont.len
//						== _h_saebus[i1]->subsc_cont.max_len)
//					return NOT_ENOUGH_MEM;
//				for (int i = _h_saebus[i1]->subsc_cont.len - 1; i >= 0; i--) {
//					if (cb == _h_saebus[i1]->subsc_cont.subscribers[i])
//						return SUBSCRIBER_ALREADY_REGISTERED;
//				}
//
//				_h_saebus[i1]->subsc_cont.subscribers[_h_saebus[i1]->subsc_cont.len] =
//						cb;
//				_h_saebus[i1]->subsc_cont.len++;
//				return NO_ERROR;
//
//			} else
//				return PUBLISHER_NOT_REGISTERED;
//		}
//
//	}
//	return EVENT_NOT_REGISTERED;
//}
//eSAESysErrorFlags SAE_removeSubscribe(uint32_t event, sae_subscriber cb) {
//	for (int i1 = 0; i1 < SAEB_MAX_EVENT; i1++) {
//		if(_h_saebus[i1])
//		if (_h_saebus[i1]->event == event) {
//			if (_h_saebus[i1]->flags != NO_PUBLISHER) {
//				if (_h_saebus[i1]->subsc_cont.len == 0)
//					return NO_ERROR;
//				for (int i = _h_saebus[i1]->subsc_cont.len - 1; i >= 0; i--) {
//					if (cb == _h_saebus[i1]->subsc_cont.subscribers[i]) { //Нашли что нужно удалить
//						if (i != _h_saebus[i1]->subsc_cont.len - 1) {
//							_h_saebus[i1]->subsc_cont.subscribers[i] =
//									_h_saebus[i1]->subsc_cont.subscribers[_h_saebus[i1]->subsc_cont.len];
//							_h_saebus[i1]->subsc_cont.subscribers[_h_saebus[i1]->subsc_cont.len] =
//									0;
//						} else
//							_h_saebus[i1]->subsc_cont.subscribers[i] = 0;
//						_h_saebus[i1]->subsc_cont.len--;
//					}
//				}
//
//				_h_saebus[i1]->subsc_cont.subscribers[_h_saebus[i1]->subsc_cont.len] =
//						cb;
//				_h_saebus[i1]->subsc_cont.len++;
//				return NO_ERROR;
//
//			} else
//				return PUBLISHER_NOT_REGISTERED;
//		}
//
//	}
//	return EVENT_NOT_REGISTERED;
//
//}
